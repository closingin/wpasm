# wpasm

Network management with wpa_supplicant


## Installation

Launch the script as root

```bash
$ chmod +x INSTALL.sh
$ sudo ./INSTALL.sh
```

The installation script will copy wpasm.sh into /opt/scripts/ and link it to /usr/bin/wpasm

## Usage

```bash
$ wpasm -h
usage: wpasm ...
```

```bash
$ wpasm -n MyWifiAp
[+] Generating configuration for MyWifiAp
[?] Passphrase for MyWifiAp : [PasswordHere]
[+] File saved in /etc/wpa_supplicant/conf/mywifiap.conf
[+] Access point set up, execute wpasm -u mywifiap as root to connect
```

```bash
$ wpasm -u mywifiap
[+] Initiating connection to ap
[+] Releasing wlp9s0
[+] Associating with MyWifiAp
[+] Waiting for dhcp on wlp9s0
[+] Connected as 192.168.1.2
```

## About

Feel free to use, edit, or share this script ! I mean, do whatever you want with it.
