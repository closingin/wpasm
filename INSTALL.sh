#!/bin/sh
#

if [ `id -u` -ne 0 ]; then
	echo "You must run this script as root"
	exit 1
fi

mkdir -p /opt/scripts/
echo "Copying to /opt/scripts/"
cp wpasm.sh /opt/scripts/
chmod +x /opt/scripts/wpasm.sh
echo "Linking to /usr/bin/wpasm"
ln -sf /opt/scripts/wpasm.sh /usr/bin/wpasm
echo "Done."
