## version 1.0

- TODO: -d to directly connect to an ap
- TODO: improve -t
- Now supports WEP and OPEN networks
- Everything has been rewritten
- Improved all displayed text
- Colors are in !
- Added ap's encryption when using -s
- Added -t option to specify ap's encryption type, defaults to WPA (only with -n)
- Added some variables that can be changed by the user
- Ap is now case-insensitive
- Fixed chmod on /etc/wpa_supplicant/conf preventing listing, fuck me.
- Bug fixes

## version 1.0a

- Switched to getopt to parse options
- Disabled long-text options
- Fixed the case of the configuration when saved, now lowercase

## version 0.8

- Fixed few bugs
- Creating a configuration file now requires root to copy to /etc/wpa_supplicant/conf/
- Edited information messages to comply with common shell scripts

## version 0.7

- First public release
- Basic features
	Connect to wpa secured networks
	Scan networks
	List saved networks (/etc/wpa_supplicant/[conf])
