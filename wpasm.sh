#!/bin/bash
#
## Author      : closingin
## Description : network management with wpa_supplicant
## Update      : Jun 08 14:44 2015 closingin
#
# Feel free to edit and share this script

# You can change these variables
WPASM_CONF="/etc/wpa_supplicant/conf"
WPASM_SOCK="/var/run/wpa_supplicant"
WPASM_WLAN=$(ls /sys/class/net/ | grep ^w)
WPASM_DHCP="dhcpcd"

wpasm_usage()
{
	echo -e "usage: wpasm [-u config_file]\t\t\tConnect to an access point using config_file"
	echo -e "             [-n ssid -t OPN|WEP|WPA[2]]\tCreate a new configuration file for given ssid"
	echo -e "             \e[00;31m[-d ssid -t OPN|WEP|WPA[2]]\tTODO: Directly connect to given ssid\e[00m"
	echo -e "             [-l]\t\t\t\tList existing configuration files"
	echo -e "             [-s]\t\t\t\tScan and display networks around you"
	echo -e "             [-h]\t\t\t\tDisplay this help"
}

wpasm_error()
{
	if [ $1 -eq 0 ] ; then
		wpasm_usage
	else
		echo -n -e "[\e[00;31m!\e[00m] "
		if [ $1 -eq 1 ] ; then
			echo "You must run the script as root to use this feature"
		elif [ $1 -eq 2 ] ; then
			echo "Please install $2"
		elif [ $1 -eq 3 ] ; then
			echo "Access point not found, maybe not in range"
		elif [ $1 -eq 4 ] ; then
			echo "$CMDARG: No such file or directory"
		elif [ $1 -eq 5 ] ; then
			echo "Encryption types supported : OPN WEP WPA WPA2"
		elif [ $1 -eq 6 ] ; then
			echo "Passphrase must be 8 to 63 characters long"
		fi
	fi
	exit 1
}

wpasm_getfile()
{
	WPASM_OPT="`[[ -f "$1" ]] && echo $1 || [[ -f "$1.conf" ]] && echo "$1.conf"`"
	[[ -z $WPASM_OPT ]] && return 1
	return 0
}

wpasm_scan()
{
	ip link set $WPASM_WLAN up
	if [ "$1" ]; then
		iwlist $WPASM_WLAN scan | grep "ESSID:" | cut -d '"' -f 2 | grep -x $1 &> /dev/null
		return $?
	else
		DEFAULT_CL=$'\e[00m'
		CYAN_CL=$'\e[00;36m'
		WPASM_REGEX_ESSID="a-zA-Z0-9_ &\*\/\(\)\.-"
		sudo iwlist $WPASM_WLAN scan | grep -E "Encryption key|ESSID:|IE: IEEE|IE: WPA" | sed "s/^ *//" | \
			# Replacing raw encryption with "readable" one
			sed "s/Encryption key:off/[OPN]/" | sed "s/Encryption key:on/[WEP]/" | sed -r "s/.*WPA([0-9]?) Version [0-9]?$/[WPA\1]/" | \
			# Removing garbage from ssid
			sed -r "s/ESSID:(\"[$WPASM_REGEX_ESSID]*\")/\1/" | \
			tr '\n' '%' | \
			# Interverting encryption<>essid
			sed -r "s/(\[OPN\]|\[WEP\])%(\"[$WPASM_REGEX_ESSID]*\")/\2%\1/g" | \
			# Get wep secured network
			sed -r "s/\[WEP\]%(\[WPA[0-9]?\])/\1/g" | \
			# Remove duplicates for wpa networks
			sed -r "s/(\[WPA[0-9]?\])%\[WPA\]/\1/g" | \
			# Display the shit
			sed -r "s/\"([$WPASM_REGEX_ESSID]*)\"%\[(WPA[0-9]?|WEP|OPN)\]%/[+] \1 [$CYAN_CL\2$DEFAULT_CL]\n/g" | \
			sed -r "/\[\+\]  \[.*/d"
	fi
}

wpasm_connect()
{
	command -v wpa_supplicant > /dev/null || wpasm_error 2 "wpa_supplicant"
	command -v $WPASM_DHCP > /dev/null || wpasm_error 2 "$WPASM_DHCP"
	echo -e "[+] \e[01mInitiating connection to ap\e[00m"
	WPASM_SSID="`cat "$WPASM_OPT" | grep ssid | cut -d '"' -f 2`"
	wpasm_scan $WPASM_SSID || wpasm_error 3
	wpa_cli -p $WPASM_SOCK -i $WPASM_WLAN terminate &> /dev/null
	echo "[+] Releasing $WPASM_WLAN"
	ip link set $WPASM_WLAN down
    # Edit this line if you change the dhcp client
    # dhcpcd   : $WPASM_DHCP -k $WPASM_WLAN
    # dhclient : $WPASM_DHCP -r $WPASM_WLAN
    # pump     : $WPASM_DHCP -i $WPASM_WLAN -r
	$WPASM_DHCP -k $WPASM_WLAN &> /dev/null
	ip link set $WPASM_WLAN up
	echo "[+] Associating with $WPASM_SSID"
	wpa_supplicant -B -Dwext -i$WPASM_WLAN -c$1 &> /dev/null
	echo "[+] Waiting for dhcp on $WPASM_WLAN"
	$WPASM_DHCP $WPASM_WLAN &> /dev/null
	echo -n "[+] Connected as "
	ip address show $WPASM_WLAN | grep -w inet | cut -d " " -f 6 | cut -d "/" -f 1
	exit 0
}

wpasm_create()
{
	echo -e "[+] \e[01mGenerating configuration for $CMDARG\e[00m"
	if [ $1 != "OPN" ] ; then
		read -s -p "[?] Passphrase for $CMDARG : " WPASM_KEY
		echo
		[[ ${#WPASM_KEY} -lt 8 ]] || [[ ${#WPASM_KEY} -gt 63 ]] && wpasm_error 6
		if [ $1 = "WEP" ] ; then
			echo "ctrl_interface=DIR=$WPASM_SOCK" > "$WPASM_CONF/$LCMDARG.conf"
			echo -e "network={\n\tssid=\"$CMDARG\"\n\tkey_mgmt=NONE\n" >> "$WPASM_CONF/$LCMDARG.conf"
			echo -e "\twep_key0=$WPASM_KEY\n\twep_tx_keyidx=0\n}" >> "$WPASM_CONF/$LCMDARG.conf"
		elif [ $1 = "WPA" ] || [ $1 = "WPA2" ] ; then
			wpa_passphrase "$CMDARG" "$WPASM_KEY" >> "$WPASM_CONF/$LCMDARG.conf"
		fi
	else
		echo "ctrl_interface=DIR=$WPASM_SOCK" > "$WPASM_CONF/$LCMDARG.conf"
		echo -e "network={\n\tssid=\"$CMDARG\"\n\tkey_mgmt=NONE\n}" >> "$WPASM_CONF/$LCMDARG.conf"
	fi
	chmod 600 "$WPASM_CONF/$LCMDARG.conf"
	echo "[+] File saved in $WPASM_CONF/$LCMDARG.conf"
	echo -e "[+] Access point set up, execute \e[00;36mwpasm -u $LCMDARG\e[00m as root to connect"
	exit 0
}

while getopts ":u:n:ls" CMD ; do
	CMDARG="$OPTARG"
	LCMDARG="`echo $CMDARG | tr '[:upper:]' '[:lower:]' | tr ' ' '-'`"
	case $CMD in
		u)
			[[ `id -u` -ne 0 ]] && wpasm_error 1
			for WPASM_DIR in / . /etc/wpa_supplicant $WPASM_CONF ; do
				wpasm_getfile "$WPASM_DIR/$LCMDARG" && wpasm_connect $WPASM_OPT
			done
			wpasm_error 4
			;;
		n)
			[[ `id -u` -ne 0 ]] && wpasm_error 1
			[[ ! -d "$WPASM_CONF" ]] && mkdir -m 755 -p $WPASM_CONF
			if [ $# -eq 2 ] ; then
				wpasm_create "WPA"
			elif [ $# -eq 4 ] && [ $3 = "-t" ] ; then
				for WPASM_ENC in OPN WPA WPA2 WEP ; do
					[[ $4 = $WPASM_ENC ]] && wpasm_create $4
				done
				wpasm_error 5
			fi
			wpasm_error 0
			;;
		s)
			[[ `id -u` -ne 0 ]] && wpasm_error 1
			echo -e "[+] \e[01mRunning network scan\e[00m"
			wpasm_scan
			exit 0
			;;
		l)
			[[ `id -u` -ne 0 ]] && wpasm_error 1
			echo -e "[+] \e[01mDisplaying list of existing networks\e[00m"
			for WPASM_FILE in $(ls $WPASM_CONF | grep -E ".conf$") ; do
				WPASM_SSID="`cat "$WPASM_CONF/$WPASM_FILE" | grep ssid | cut -d '"' -f 2`"
				echo -e "[+] $WPASM_SSID [\e[00;32mwpasm -u ${WPASM_FILE%.conf}\e[00m]"
			done
			for WPASM_FILE in $(ls /etc/wpa_supplicant/ | grep -E ".conf$") ; do
				WPASM_SSID="`cat "/etc/wpa_supplicant/$WPASM_FILE" | grep ssid | cut -d '"' -f 2`"
				echo -e "[+] $WPASM_SSID [\e[00;32mwpasm -u ${WPASM_FILE%.conf}\e[00m]"
			done
			exit 0
			;;

		*)
			wpasm_usage
			exit 0
			;;
	esac
done
